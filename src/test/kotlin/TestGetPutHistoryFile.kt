import data.HistoryItem
import data.ListItem
import data.getHistoryFile
import data.putHistoryFile
import java.nio.file.Files
import java.time.OffsetDateTime
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class TestGetPutHistoryFile {

    @Test
    fun testEmptyStartAndFinish() {
        val file = Files.createTempFile("history", ".json").toFile()
        val history = getHistoryFile(file)
        assertTrue(history.isEmpty())
        history.putHistoryFile(file)
        assertTrue(getHistoryFile(file).isEmpty())
    }

    @Test
    fun testEmptyStartFilledFinish() {
        val file = Files.createTempFile("history", ".json").toFile()
        val history = getHistoryFile(file)
        assertTrue(history.isEmpty())
        history.add(HistoryItem(1, OffsetDateTime.now(), listOf(ListItem(1,"1"))))
        history.putHistoryFile(file)
        assertEquals(history, getHistoryFile(file))
    }

    @Test
    fun testAddedItems() {
        val file = Files.createTempFile("history", ".json").toFile()
        val history = getHistoryFile(file)
        assertTrue(history.isEmpty())
        history.add(HistoryItem(1, OffsetDateTime.now(), listOf(ListItem(1,"1"))))
        history.putHistoryFile(file)
        val newHistory = getHistoryFile(file)
        assertEquals(history, newHistory)
        newHistory.add(HistoryItem(2, OffsetDateTime.now(), listOf()))
        newHistory.putHistoryFile(file)
        assertEquals(newHistory, getHistoryFile(file))

    }
}