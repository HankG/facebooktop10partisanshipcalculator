import data.HistoryItem
import data.ListItem
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.time.OffsetDateTime
import kotlin.test.Test
import kotlin.test.assertEquals

class HistoryFileSerializationTest {

    @Test
    fun testRoundTripSerializationJson() {
        val values = listOf(
            HistoryItem(1, OffsetDateTime.now(), listOf()),
            HistoryItem(2, OffsetDateTime.now(), listOf(ListItem(1, "First"), ListItem(2, "Second")))
        )

        val json = Json { prettyPrint = true }.encodeToString(values)
        val fromJson = Json { }.decodeFromString<List<HistoryItem>>(json)
        println(json)
        assertEquals(values, fromJson)
    }
}