import data.ListItem
import serialization.TopTenExtractor
import kotlin.test.Test
import kotlin.test.assertEquals

class TopTenExtractorTest {
    @Test
    fun testExtractorWithSampleString() {
        val tweetText = """
            The top-performing link posts by U.S. Facebook pages in the last 24 hours are from:

            1. The Pioneer Woman
            2. Fox News
            3. Dan Bongino
            4. USA Patriots for Donald Trump
            5. Robert Reich
            6. Dan Bongino
            7. Cuteness overload
            8. Go Awesome Animals
            9. Ben Shapiro
            10. Dinesh D'Souza
        """.trimIndent()

        val expected = listOf<ListItem>(
            ListItem(1, "The Pioneer Woman"),
            ListItem(2, "Fox News"),
            ListItem(3, "Dan Bongino"),
            ListItem(4, "USA Patriots for Donald Trump"),
            ListItem(5, "Robert Reich"),
            ListItem(6, "Dan Bongino"),
            ListItem(7, "Cuteness overload"),
            ListItem(8, "Go Awesome Animals"),
            ListItem(9, "Ben Shapiro"),
            ListItem(10, "Dinesh D'Souza"),
        )
        val actual = TopTenExtractor.extract(tweetText)
        assertEquals(expected, actual)
    }

    @Test
    fun testEmptyString() {
        assertEquals(listOf(), TopTenExtractor.extract(""))
    }


    @Test
    fun testNonListTweetString() {
        val tweetText = """
        Note: a post by UNICEF was excluded from this list because it was boosted by a 
        Facebook Covid-19 info panel.
        """.trimIndent()
        assertEquals(listOf(), TopTenExtractor.extract(tweetText))
    }

    @Test
    fun testInterspersed() {
        val tweetText = """
            The top-performing link posts by U.S. Facebook pages in the last 24 hours are from:

            1. The Pioneer Woman
            some other text
            2. Fox News
            and another
            3. Dan Bongino
            4. USA Patriots for Donald Trump
            5. Robert Reich
            6. Dan Bongino
            7. Cuteness overload
            8. Go Awesome Animals
            9. Ben Shapiro
            10. Dinesh D'Souza
            
            more text here
        """.trimIndent()

        val expected = listOf<ListItem>(
            ListItem(1, "The Pioneer Woman"),
            ListItem(2, "Fox News"),
            ListItem(3, "Dan Bongino"),
            ListItem(4, "USA Patriots for Donald Trump"),
            ListItem(5, "Robert Reich"),
            ListItem(6, "Dan Bongino"),
            ListItem(7, "Cuteness overload"),
            ListItem(8, "Go Awesome Animals"),
            ListItem(9, "Ben Shapiro"),
            ListItem(10, "Dinesh D'Souza"),
        )
        val actual = TopTenExtractor.extract(tweetText)
        assertEquals(expected, actual)
    }

}