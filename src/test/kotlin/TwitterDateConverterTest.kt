import org.junit.Test
import twitter.TwitterDateConverter
import java.time.OffsetDateTime
import java.time.ZoneOffset
import kotlin.test.assertEquals

class TwitterDateConverterTest {
    @Test
    fun testExampleDate() {
        val text = "Wed May 23 06:01:13 +0000 2007"
        val expected = OffsetDateTime.of(2007, 5, 23, 6, 1, 13, 0, ZoneOffset.UTC)
        val actual = TwitterDateConverter.fromTwitterDate(text)
        assertEquals(expected, actual)
    }
}