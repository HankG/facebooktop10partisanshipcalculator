import com.codingfeline.twitter4kt.core.ConsumerKeys
import com.codingfeline.twitter4kt.core.Twitter
import com.codingfeline.twitter4kt.core.model.oauth1a.AccessToken
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.file.Files
import java.nio.file.Path

@Serializable
data class AppSettings(
    val maxTwitterPerPull: Int,
    val twitterAccountToRead: String,
    val mappingPath: String,
    val datasetPath: String,
    val outputFolder: String,
)

@Serializable
data class TwitterSettings(
    val consumerKey: String,
    val consumerSecret: String,
    val tokenData: TokenData
)

@Serializable
data class TokenData(
    val token: String,
    val secret: String,
    val userId: String,
    val screenName: String
)

fun TwitterSettings.buildTwitterObject() = Twitter {
    consumerKeys = ConsumerKeys(
        key = consumerKey,
        secret = consumerSecret
    )
}

fun TwitterSettings.buildAccessToken() = AccessToken(
    token = tokenData.token,
    secret = tokenData.secret,
    userId = tokenData.userId,
    screenName = tokenData.screenName
)

fun twitterSettingsFromFile(file: File): TwitterSettings =
    Json { }.decodeFromString(Files.readString(file.toPath()))

fun appSettingsFromFile(file: File): AppSettings =
    Json { }.decodeFromString(Files.readString(file.toPath()))