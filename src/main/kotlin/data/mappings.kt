package data

import com.opencsv.CSVReader
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

fun getMappingFile(file: File): Map<String, Double> {
    if (!file.exists()) return mapOf()
    val reader = BufferedReader(FileReader(file))
    val entries = mutableMapOf<String, Double>()
    reader.use {
        val csvReader = CSVReader(it)
        var record = csvReader.readNext()
        while (record != null) {
            if (record.size >= 2) {
                val name = record[0]
                val rank = record[1].toDouble()
                entries[name] = rank
            }
            record = csvReader.readNext()
        }
        csvReader.close()
    }

    return entries
}