package data

import serialization.OffsetDateTimeSerializer
import com.codingfeline.twitter4kt.v1.model.status.Tweet
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import serialization.TopTenExtractor
import twitter.TwitterDateConverter
import java.io.File
import java.nio.file.Files
import java.time.OffsetDateTime


@Serializable
data class HistoryItem(
    val twitterId: Long,
    @Serializable(with = OffsetDateTimeSerializer::class)
    val date: OffsetDateTime,
    val ranks: List<ListItem>,
) {
    companion object {
        val NOT_SET = HistoryItem(-1, OffsetDateTime.MIN, listOf())
    }
}

fun getHistoryFile(file: File): MutableList<HistoryItem> {
    if (!file.exists()) return mutableListOf()
    val jsonData = Files.readString(file.toPath()).trim()
    if (jsonData.isEmpty()) return mutableListOf()
    return Json { }.decodeFromString(jsonData)
}

fun List<HistoryItem>.putHistoryFile(file: File) {
    val json = Json { prettyPrint = true }.encodeToString(this)
    Files.writeString(file.toPath(), json)
}

fun Tweet.toHistoryItem(): HistoryItem {
    val items = TopTenExtractor.extract(this.fullText ?: "")
    val timestamp = TwitterDateConverter.fromTwitterDate(this.createdAt)
    val id = this.id

    return HistoryItem(id, timestamp, items)
}
