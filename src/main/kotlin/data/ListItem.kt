package data

import kotlinx.serialization.Serializable

@Serializable
data class ListItem(
    val rank: Int = -1,
    val text: String = "",
)
