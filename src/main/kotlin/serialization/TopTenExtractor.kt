package serialization

import data.ListItem

object TopTenExtractor {
    fun extract(text: String) = text.split("\n")
        .filter { it.isNotEmpty() }
        .filter { it[0].isDigit() }
        .map { convertLineToListItem(it) }
        .toList()

    private fun convertLineToListItem(line: String): ListItem {
        val periodIndex = line.indexOf('.')
        val rankEnd = if(periodIndex < 0) line.indexOf(',') else periodIndex //typo of comma for period sometimes
        //Sometimes missing entry altogether
        if (rankEnd < 0) {
            return ListItem()
        }
        val rank = line.substring(0, rankEnd).toInt()
        val name = line.substring(rankEnd + 1).trim()
        return ListItem(rank, name)
    }
}