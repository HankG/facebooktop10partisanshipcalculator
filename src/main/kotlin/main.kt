import analysis.DayScorer
import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.core.findOrSetObject
import com.github.ajalt.clikt.core.requireObject
import com.github.ajalt.clikt.core.subcommands
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.types.file
import data.getHistoryFile
import data.putHistoryFile
import twitter.TwitterPuller
import java.lang.IllegalArgumentException
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.exitProcess

class MainProgram : CliktCommand(allowMultipleSubcommands = true, printHelpOnEmptyArgs = true) {
    val appSettingsFile by argument(name = "app-settings", "Path to Application settings JSON")
        .file(mustExist = true, mustBeReadable = true)
    val appSettings by findOrSetObject { appSettingsFromFile(appSettingsFile) }

    override fun run() {
        if (Files.notExists(Paths.get(appSettings.outputFolder))) {
            throw IllegalArgumentException("Need to have an output folder specified")
        }
    }

}

class Analyze : CliktCommand(name = "analyze") {
    val appSettings by requireObject<AppSettings>()
    override fun run() {
        echo("Performing analysis")
        val history = getHistoryFile(Paths.get(appSettings.datasetPath).toFile())
        echo("Processing ${history.size} entries")
        DayScorer(appSettings).score(history)
        echo("Complete, results store in: ${appSettings.outputFolder}")
    }

}

class PullFromTwitter : CliktCommand(name = "twitter-pull") {
    val appSettings by requireObject<AppSettings>()
    val twitterSettingsFile by argument(name = "twitter-settings", help = "filepath to Twitter configuration JSON")
        .file(mustExist = true, mustBeReadable = true)

    override fun run() {
        val twitterSettings = twitterSettingsFromFile(twitterSettingsFile)
        val puller = TwitterPuller(twitterSettings, appSettings)
        val historyFile = Paths.get(appSettings.datasetPath).toFile()

        val history = getHistoryFile(historyFile).toMutableSet()
        val latestId = history.maxByOrNull { it.twitterId }?.twitterId?.toString() ?: "1"
        val earliestId = history.minByOrNull { it.twitterId }?.twitterId?.toString() ?: "1"

        echo("Searching for tweets newer than $latestId")
        val newNewer = puller.getNewerTweets(latestId)
        echo("Searching for tweets older than $earliestId")
        val newOlder = puller.getOlderTweets(earliestId)

        echo("Initial history size: ${history.size}")
        echo("Newer tweets size: ${newNewer.size}")
        echo("Older tweets size: ${newOlder.size}")

        history.addAll(newNewer)
        history.addAll(newOlder)
        history.toList().sortedBy { it.twitterId }.putHistoryFile(historyFile)
        echo("New history size: ${history.size}")

        exitProcess(0)
    }

}

fun main(args: Array<String>) = MainProgram().subcommands(Analyze(), PullFromTwitter()).main(args)