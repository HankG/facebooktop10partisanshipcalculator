package twitter

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

object TwitterDateConverter {
    private const val TWITTER_DATE_PATTERN = "EEE MMM dd HH:mm:ss ZZZ yyyy"
    private val FORMAT = DateTimeFormatter.ofPattern(TWITTER_DATE_PATTERN)
    
    fun fromTwitterDate(date: String): OffsetDateTime = OffsetDateTime.parse(date, FORMAT)
}