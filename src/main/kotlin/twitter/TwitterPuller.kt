package twitter

import AppSettings
import TwitterSettings
import buildAccessToken
import buildTwitterObject
import com.codingfeline.twitter4kt.core.Twitter
import com.codingfeline.twitter4kt.core.getOrThrow
import com.codingfeline.twitter4kt.core.model.oauth1a.AccessToken
import com.codingfeline.twitter4kt.core.session.ApiClient
import com.codingfeline.twitter4kt.core.startSession
import com.codingfeline.twitter4kt.v1.api.statuses.statuses
import com.codingfeline.twitter4kt.v1.api.statuses.userTimeline
import data.HistoryItem
import data.toHistoryItem
import kotlinx.coroutines.runBlocking

class TwitterPuller(twitterSettings: TwitterSettings, val appSettings: AppSettings) {
    private val twitter: Twitter = twitterSettings.buildTwitterObject()
    private val accessToken: AccessToken = twitterSettings.buildAccessToken()
    private val client: ApiClient = twitter.startSession(accessToken)

    fun getNewerTweets(sinceId: String): List<HistoryItem> {
        return runBlocking {
            val result = client.statuses.userTimeline(
                screenName = appSettings.twitterAccountToRead,
                count = appSettings.maxTwitterPerPull,
                includeEntities = true,
                excludeReplies = true,
                sinceId = sinceId
            )
            val newItems = result.getOrThrow().map { it.toHistoryItem() }
            newItems
        }
    }

    fun getOlderTweets(maxId: String): List<HistoryItem> {
        return runBlocking {
            val result = client.statuses.userTimeline(
                screenName = appSettings.twitterAccountToRead,
                count = appSettings.maxTwitterPerPull,
                includeEntities = true,
                excludeReplies = true,
                maxId = maxId
            )
            val newItems = result.getOrThrow().map { it.toHistoryItem() }
            newItems
        }
    }
}