package analysis

import AppSettings
import data.HistoryItem
import data.getMappingFile
import java.nio.file.Files
import java.nio.file.Paths
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class DayScorer(appSettings: AppSettings) {
    val mappings = getMappingFile(Paths.get(appSettings.mappingPath).toFile())
    val outputFolder = appSettings.outputFolder

    fun score(items: List<HistoryItem>) {
        val notGoodValue = -1000.0
        val actualMinValue = -10.0
        val inauguration2021 = LocalDate.of(2021, 1, 21)

        val unknownKeys = mutableSetOf<String>()
        val dailyScores = items.map { historyItem ->
            val scores = historyItem.ranks
                .map {
                    val value = if (it.text == "The White House") {
                        if (historyItem.date.toLocalDate().isAfter(inauguration2021)) {
                            -10.0
                        } else {
                            10.0
                        }
                    } else {
                        mappings.getOrDefault(it.text, notGoodValue)
                    }
                    if (value < actualMinValue) {
                        unknownKeys.add(it.text)
                    }
                    value
                }
                .filter { it >= actualMinValue }
            val score = scores.sum() / 10.0
            val date = with(historyItem.date) {
                LocalDate.of(year, month, dayOfMonth)
            }
            "$date, $score"
        }.joinToString(System.lineSeparator())

        val timestamp = LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME)
        val rankFile = Paths.get(outputFolder, "ranks_$timestamp.csv")
        val unknownsFile = Paths.get(outputFolder, "unknowns_$timestamp.csv")
        Files.writeString(rankFile, dailyScores)
        Files.writeString(unknownsFile, unknownKeys.joinToString(System.lineSeparator()))
    }
}