# Facebook Top 10 Partisanship Calculator

Tools, raw data, and results for trying to estimate and trend the 
partisan composition of Facebook's daily top 10 posts from a US 
politics point of view.

Any custom source code generated for running or processing these results 
is released under an AGPL 3.0 license.

All data is being released under a Creative Commons Attribution (CC-BY)
4.0 license.

## Implementation

Base data for this analysis comes from the
[Facebook Top 10](https://twitter.com/FacebooksTop10) Twitter feed
that is managed by [Kevin Roose](https://twitter.com/kevinroose). That
data set is a distillation of daily statistics from 
[CrowdTangle](https://www.crowdtangle.com/). 

The [Kotlin](https://kotlinlang.org/) based application queries for new
Tweets to incorporate that into it's local data set. It analyzes the data set
by looking up a "partisanship score" for each of the authors in that day's
top 10. It then comes up with a composite score by summing up those values.

The *Partisanship Score* is a subjective measure of how partisan and to 
which political side of the spectrum a poster is from the point of view of
United States politics. The scale goes from +10 to -10, where +10 is as far
as possible right wing partisanship and -10 is as far left. So for example
the RNC's Facebook account would have a +10 value while the DNC's Facebook
account would have -10. ESPN on the other hand would be a 0. This obviously
mis-scores non-political posts by politicians/political campaigns/etc. while
missing political posts by non-political people. That is part of the 
subjectiveness that needs tweaking. Ideally though if the top ten had a
couple very right wing posts, a bunch of non-political posts, and several 
moderately left wing posts it would net to near neutrality. 

Whenever unknown names are presented during analysis
it dumps those to a separate file and assumes a non-partisanship value of 
zero for that particular run. The operator can then keep the table updated.

## Building

To build you'll need a JDK 11 or higher installed on your system. I use 
OpenJDK so this is what it was tested on. You'll also need a git client if you 
are checking out the source directly rather than downloading a zip file of it.

To go from a clean checkout to a self contained Java Archive file:

```bash
git clone https://gitlab.com/HankG/facebooktop10partisanshipcalculator
cd facebooktop10partisanshipcalculator
./gradlew shadowJar
```

The executable will then be in the `./build/libs` folder for use.

## Usage

To use the program invoke from the command line using `java -jar` such as below 
where we use the `--help` flag to list the help or when omitting any arguments:

```bash
$ java -jar build/libs/FacebookTopTenPartisanshipCalculator-1.0-SNAPSHOT-all.jar

Usage: main-program [OPTIONS] app-settings COMMAND [ARGS]...

Options:
  -h, --help  Show this message and exit

Arguments:
  app-settings  Path to Application settings JSON

Commands:
  analyze
  twitter-pull
```

The one necessary argument is the app-settings path which points to the JSON
file with your configuration, an example can be found 
[here](https://gitlab.com/HankG/facebooktop10partisanshipcalculator/-/blob/main/data/app_settings.json). 
The two modes one can run this in is the `twitter-pull` mode which connects to Twitter and
pulls down the latest tweets. You will need your own Twitter API application
and user credentials. That will be an additional JSON file that you will 
need to have available confirming with the example format presented 
[here](https://gitlab.com/HankG/facebooktop10partisanshipcalculator/-/blob/main/data/twitter_security_settings.json).

## Examples 

### Executing pulling down new tweets

```bash
java -jar build/libs/FacebookTopTenPartisanshipCalculator-1.0-SNAPSHOT-all.jar \
      data/app_settings.json twitter-pull ~/tsettings.json
```

### Executing performing a new analysis

```bash
java -jar build/libs/FacebookTopTenPartisanshipCalculator-1.0-SNAPSHOT-all.jar \
      data/app_settings.json analyze
```

## Data

The input and output data can be found in this project's 
[`data` folder](https://gitlab.com/HankG/facebooktop10partisanshipcalculator/-/tree/main/data).
In there you will find:
* **app_settings.json**: The settings file used to execute the ingestion and analysis runs
* **twitter_security_settings.json**: The template for filling in your own Twitter API 
  credentials if you wish to update the data set on your own side
* **input/dataset.json**: The current latest copy of the data set which will mirror each 
  of the Tweets with "Top 10 Facebook" data in it. 
* **input/mappings.csv**: This is used to map the poster name to a partisanship score (see above).
* **output**: The folder where all of the results are stored on execution using the
  standard `app_settings.json` file. This is where the up to date data sets I process will be
  stored as well.
  

